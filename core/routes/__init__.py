from core.routes.church.blueprint import church_api, church_blueprint

blueprints = [
    church_blueprint
]

apis = [
    church_api
]