from flask import Blueprint
from flask_restful import Api

from core.routes.church.church_routes import Church


church_blueprint = Blueprint("churchs", __name__, url_prefix='/churchs')
church_api = Api(church_blueprint)

church_api.add_resource(Church, "/me")
